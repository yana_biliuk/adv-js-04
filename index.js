function fetchFilms() {
  const url = "https://ajax.test-danit.com/api/swapi/films";

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
      return Promise.all(data.map((film) => fetchCharacters(film.characters)));
    })
    .then((data) => {
      getCharacters(data);
    })
    .catch((error) => {
      console.error("Помилка отримання списку фільмів:", error);
    });
}

function displayFilms(data) {
  const ul = document.getElementById("ul");

  data.forEach(({ episodeId, name, openingCrawl}) => {
    
    const li = document.createElement("li");
    li.innerHTML = ` <strong>Епізод ${episodeId}:</strong> ${name} <h3>Короткий зміст:</h3> <p>${openingCrawl}</p>`;

    ul.append(li);
  });
}

function fetchCharacters(characters) {
  return Promise.all(
    characters.map((characterUrl) =>
      fetch(characterUrl).then((response) => response.json())
    )
  );
}

function getCharacters(data) {
  const films = document.querySelectorAll("#ul li");

  data.forEach((film, index) => {
    const charactersList = document.createElement("ul");
    charactersList.innerHTML = "<h4>Персонажі:</h4>";
    film.forEach((character) => {
      const li = document.createElement("li");
      li.textContent = character.name;
      charactersList.append(li);
    });

    films[index].append(charactersList);
  });
}

window.onload = fetchFilms;
